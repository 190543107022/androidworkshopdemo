package com.utsav.androidworkshopdemo;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    //Get Data From Other Activity
    String firstName, phone, email, temp;

    //Display All Data
    TextView activity_second_tvDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        init();
        process();
        listeners();
    }

    private void init() {
        //Get Data From The Other Activity
        firstName = getIntent().getStringExtra("FirstName");
        phone = getIntent().getStringExtra("Phone");
        email = getIntent().getStringExtra("Email");
        temp = firstName + " " + phone + " " + email;
        //Initialize TextView For Display Data
        activity_second_tvDisplay = findViewById(R.id.activity_second_tvDisplay);
    }

    private void process() {
        //Set Data Into TextView
        activity_second_tvDisplay.setText(temp);
    }

    private void listeners() {
    }
}