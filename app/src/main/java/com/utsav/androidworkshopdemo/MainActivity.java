package com.utsav.androidworkshopdemo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    //Cast Number Into Integer
    long phoneInteger;

    //TextView For Display Data Of Entered FirstName And LastName
    TextView activity_main_tvDisplay;

    //EditText For Entered Data Get From User
    EditText activity_main_etFirstName, activity_main_etPhone, activity_main_etEmail;

    //Button For Get Submit Button Click Event
    Button activity_main_btnSubmit;

    //String For Temporary Data Store
    String firstName, email, phone, temp;

    //Exit On Double BackPressed
    boolean doubleBackToExitPressedOnce = false;

    //Radio Group
    RadioGroup rg_Gender;

    //Radio Button
    RadioButton rb_Male, rb_Female;

    //CheckBox
    CheckBox chb_Cricket, chb_Football, chb_Hockey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        process();
        listeners();
    }

    private void init() {

        //Initialize EditText For Get Data From User
        activity_main_etFirstName = findViewById(R.id.activity_main_etFirstName);
        activity_main_etPhone = findViewById(R.id.activity_main_etPhone);
        activity_main_etEmail = findViewById(R.id.activity_main_etEmail);

        //Initialize TextView For Display Data
        activity_main_tvDisplay = findViewById(R.id.activity_main_tvDisplay);

        //Initialize Button For Get Button Events
        activity_main_btnSubmit = findViewById(R.id.activity_main_btnSubmit);

        //Radio Group
        rg_Gender = findViewById(R.id.rg_Gender);

        //Radio Button
        rb_Male = findViewById(R.id.rb_Male);
        rb_Female = findViewById(R.id.rb_Female);

        //CheckBox
        chb_Cricket = findViewById(R.id.chb_Cricket);
        chb_Football = findViewById(R.id.chb_Football);
        chb_Hockey = findViewById(R.id.chb_Hockey);
    }

    private void process() {
    }

    private void listeners() {

        //Submit Button Click Listeners
        activity_main_btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Get Data From EditText Into String
                firstName = activity_main_etFirstName.getText().toString();
                email = activity_main_etEmail.getText().toString();
                phone = activity_main_etPhone.getText().toString();

                //Cast Into Long Integer
                phoneInteger = Long.parseLong(phone);

                //Temporary Data Manipulate
                temp = firstName + " " + email + " " + phone;

                Toast.makeText(MainActivity.this, temp, Toast.LENGTH_SHORT).show();

                Toast.makeText(MainActivity.this, "Converted Number Into Long Integer : " + phoneInteger, Toast.LENGTH_SHORT).show();

                //Set Data Into Display TextView
                activity_main_tvDisplay.setText(temp);

                Intent i = new Intent(MainActivity.this, SecondActivity.class);
                i.putExtra("FirstName", firstName);
                i.putExtra("Email", email);
                i.putExtra("Phone", phone);
                startActivity(i);

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
}